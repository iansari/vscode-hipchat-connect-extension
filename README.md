# HipChat Connect Extension

## Features

Auto-complete and schema validation for your HipChat Connect add-on descriptor.

Add new extensions to the HipChat Connect add-on descriptor effortlessly by using snippets for webhooks, actions, glances and any other module type.

Need the JSON for an Activity card in your JS code? Use one of the card snippets included in this extension.

## Usage

### Add-on JSON descriptor

![Descriptor Editing](https://bytebucket.org/pstreule/vscode-hipchat-connect-extension/raw/master/images/descriptor.gif)

* Auto-complete and schema validation are enabled for all `atlassian-connect.json` files.
* Snippets: Start typing the module name and choose one of these snippets:
    * `action-definition`
    * `dialog-definition`
    * `externalPage-definition`
    * `glance-definition`
    * `webPanel-definition`
    * `webook-definition`

### Cards

![Descriptor Editing](https://bytebucket.org/pstreule/vscode-hipchat-connect-extension/raw/master/images/card-snippets.gif)

* Snippets: Choose one of these snippets for various card types (enabled for JavaScript files):
    * `card-activity`
    * `card-application`
    * `card-image`
    * `card-link`

## Contact

Found a bug? Ideas for improvements? Raise an issue (or even better: a PR) on https://bitbucket.org/pstreule/vscode-hipchat-connect-extension