# Welcome to the HipChat Connect Extension

## What's in the folder
* This folder contains all of the files necessary for your extension.
* `package.json` - this is the manifest file that defines the location of the snippet file.
and specifies the language of the snippets.
* `input/*.json` - the snippet sources: if you want to add a snippet, edit these files and run `npm run convert`.
* `snippets/*.json` - the files containing the generated snippets. Don't edit these directly.
* `images` - resources for the Marketplace listing and README

## Get up and running straight away
* Press `F5` to open a new window with your extension loaded.
* Create a new file with a file name suffix matching your language.
* Verify that your snippets are proposed on intellisense.

## Make changes
* Edit the files in `input/*.json` and run `npm run convert`. The conversion step simplifies authoring JSON snippets and takes care of the encoding.
* You can relaunch the extension from the debug toolbar after making changes to the files listed above.
* You can also reload (`Ctrl+R` or `Cmd+R` on Mac) the VS Code window with your extension to load your changes.

## Install your extension
* To start using your extension with Visual Studio Code copy it into the <user home>/.vscode/extensions folder and restart Code.
* To share your extension with the world, read on https://code.visualstudio.com/docs about publishing an extension.