/**
 * Convert JSON snippets into the string array format expected by VSCode.
 * Use `npm run convert` in the root of this project to run this script.
 */

var fs = require('fs');

function convertValue(key, value) {
    if (key === 'body' && typeof value === 'object') {
        return JSON.stringify(value, null, 4);
    }
    return value;
}

function convert(input, output) {
    var content = fs.readFileSync(input, 'utf8');
    var source = JSON.parse(content);
    var target = JSON.stringify(source, convertValue, 4);
    fs.writeFileSync(output, target, 'utf8');
    console.log('Converted ' + input);
}

convert('input/modules.json', 'snippets/modules.json');
convert('input/cards.json', 'snippets/cards.json');